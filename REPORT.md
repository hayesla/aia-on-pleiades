---
documentclass: scrartcl
title: SDO Data Analysis at Scale with Python on NASA Pleiades
subtitle: NAS SDO Data Service Progress Report
author:
- Will Barnes (BAERI/LMSAL)
- Mark Cheung (LMSAL)
- Arthur Amezcua (Stanford University)
- Herbert Yeung (ARC/NASA Ames)
#- Monica Bobra (Stanford University)
geometry:
- margin=0.5in
header-includes:
- \usepackage{hyperref}
- \hypersetup{colorlinks=true,linkcolor=blue,filecolor=magenta,urlcolor=cyan}
bibliography: references.bib
---
We report on the progress of the "AIA Data Analysis at Scale" component of the NAS SDO Data Service project. Thus far, we have demonstrated that co-locating SDO data with high-performance computational resources, in this case NASA's HECC, is a viable approach for delivering *a scalable, interactive analysis platform*.  This approach of "bringing the compute to the data" has proved very successful in other communities, e.g. the [Pangeo project for geoscience](http://pangeo.io/). The progress detailed below and the included examples are an important step towards providing a community platform for interactive SDO data analysis at scale. The accompanying poster, presented at the 2019 Fall AGU Meeting, provides additional details on the example science applications.

## Infrastructure

We faced a number technical, infrastructure-related challenges during this project. Most of these challenges were related to (1) the search and retrieval of data and (2) the parallelization of work across the cluster. We have documented our current progress in addressing these challenges below.

### Data Search and Retrieval

* Data Record Management System (DRMS) installed on `dfe1` node
* 400 TB of disk space allocated for SDO data
* Data transferred from Stanford on "as-needed" basis, i.e. motivated by the current science applications
* Adapted existing [`drms`](https://github.com/sunpy/drms/) Python package [@glogowski_drms_2019] to query records at Ames

### Distributed Python Workflows

* Documented and semi-automated workflow for setting up Jupyter notebook on NASA Pleiades. See [gitlab.com/wtbarnes/aia-on-pleiades](https://gitlab.com/wtbarnes/aia-on-pleiades).
* Demonstrated that the widely-popular and well-supported [Dask](https://docs.dask.org/en/latest/) Python package can be used to distribute work over hundreds of nodes, thousands of cores.
  * [`dask-jobqueue`](https://jobqueue.dask.org/en/latest/) -- parallelize workloads over many jobs using existing PBS scheduling infrastructure
  * [`dask-mpi`](http://mpi.dask.org/en/latest/) -- parallelize work over many nodes when submitting only a single job; appropriate when using queues limited to a single job
* Effectively used legacy NAS Merope cluster
  * Approximately 1800 repurposed Intel Westmere processors (Intel Xeon X5670, 12 cores, 48 GB memory each)
  * Often underutilized by more traditional HPC workloads due to limited memory, reliability issues
  * Ideal for parallelized data processing workflows which prioritize availability, number of cores over reliability and memory limits of individual workers
* Experimented with Pleiades, Electra clusters (various node types)
  * Found that **heavier traffic on these systems made interactive work difficult**
  * Exploratory work is hampered if one has to wait hours for resources to become available
  * Single-job `devel` queue a possible solution -- higher availability, short wait times

The *interactive* component of these proposed workflows is critical. We have developed our use cases entirely in Jupyter notebooks such that users are (ideally) not forced to think about the details of how their computation is distributed. Additionally, this scalability permits *exploratory* analysis of large data sets that is not feasible on even the most powerful local workstations and enables end users to answer new questions about their data.

Note that these interactive workflows are fundamentally different to typical HPC workloads which may require days or even weeks to processes. Our proposed workflows enable users to treat these resources as their "personal supercomputer," albeit for relatively shorter periods of time.

## Applications

To prove the viability of this infrastructure, we developed several example applications to show how scalable, high-performance computing (HPC) can maximize the scientific return of the SDO data set. These example applications are organized in a series of Jupyter notebooks and are available [here](https://gitlab.com/wtbarnes/aia-on-pleiades/tree/master/notebooks/tidy).

### Creating Higher Level Data Products

* Scientific analysis across multiple AIA channels requires converting "level 1" AIA images (those delivered by JSOC) to "level 1.5"
  * Remove the roll angle to align solar north with the $y-$axis of the image
  * Recenter image such that the center of the Sun is at the center of the pixel grid
  * Rescale image such that the resolution is 0.6"-per-pixel for all channels
* Adapted existing "prep" routine from [`sunpy`](https://docs.sunpy.org/en/stable/) package into [`aiapy`](https://gitlab.com/LMSAL_HUB/aia_hub/aiapy) package
* Demonstrated that applying this operation to many images scales trivially over thousands of cores as each image is processed independently of the other (i.e. "embarassingly parallel")
* Estimate that, given continuous access to every node on Merope, **we could process all current EUV imaging data to level 1.5 in approximately 2 days, including reading data from disk**
* Once processed to level 1.5, images can be co-aligned (e.g. through cross-correlation, removing solar rotation) and "stacked" into three-dimensional data "cubes"
  * Very common workflow for many different analyses
  * Data can be large (e.g. ~50 GB for 12 h of an AR cutout), making them difficult to fit into memory, perform operations on
  * **Demonstrated that data cubes can be processed in parallel using high-level, array-like operations**
  * [`ndcube`](https://docs.sunpy.org/projects/ndcube/en/stable/) Python package provides expressive, powerful syntax for coordinate aware operations, trivially parallelized with Dask (see example notebooks)
  * Built a small set of tools on top of `ndcube` as a protoype for distributed analysis of AIA data cubes; see [gitlab.com/wtbarnes/aiacube](https://gitlab.com/wtbarnes/aiacube).

### Science Use Cases

The science applications are more fully explained in the attached poster from the 2019 AGU Fall Meeting and are demonstrated in the [example notebooks](https://gitlab.com/wtbarnes/aia-on-pleiades/tree/master/notebooks/tidy). They are summarized here:

* Tracking global EUV waves across the entire solar disk at full spatial and temporal resolution using the running ratio technique of @liu_truly_2018
* Analyzing active region cooling patterns through time lag analysis method of @viall_evidence_2012
* Automatic detection of sunspots in HMI continuum images using the algorithm of @watson_modelling_2009 for the entire duration of the SDO mission
* Parallelized calculation of space weather parameters of @bobra_helioseismic_2014 for machine learning applications

## Community Interactions

Throughout this project, we solicited feedback from the solar community and interacted with others outside of the solar community who have developed similar types of workflows for different science applications:

* **Python in Astronomy Conference** -- presented a live demo of the global EUV wave application; discussed challenges of hosted science platforms with colleagues from LSST and JWST collaborations, both of whom are developing similar platforms given the large volume of data that each telescope will produce
* **Pangeo Community Meeting** -- interacted with climate modellers, oceanographers, earth scientists who all face same challenges regarding data computation; enhanced visibility of the solar community within the Pangeo community with potential for future collaboration on HPC infrastructure
* **Python in Heliophysics Community Meeting** -- presented live demo and lead informal discusssion of a hosted computing platform for heliophysics; interest from both remote sensing and *in-situ* communities
* **American Geophysical Union Meeting** -- presented poster (see attached) on material summarized here; based on interactions, there is clear interest in a publicly-available hosted computing platform for SDO data

The open source scientific Python community, particularly the SunPy community, has been *critical* to the progress of this project and will be **an essential component of any scalable computing platform for SDO data**. We benefitted from and interacted extensively with the following projects:

* `sunpy` -- for general solar physics functionality (e.g. reading/writing data, coordinate systems)
* `drms` -- for querying the netDRMS database installed at Ames
* `ndcube` -- for coordinate-aware manipulation of AIA data cubes
* `dask-jobqueue` -- for scalable computation with HPC schedulers; see [this proposed contribution](https://github.com/dask/dask-jobqueue/pull/370) for an example of how this project has contributed back to the communities it has benefitted from. 
* `aiapy` -- A Python package for analyzing AIA data. This was developed at LMSAL during the course of this project.

## References
