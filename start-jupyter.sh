#!/bin/bash

if [[ $* == *--use-ip* ]]; then
    IP=$(hostname -i)  # in cases where the notebook has to be started not on localhost, e.g. rfe
else
    IP=127.0.0.1
fi
module purge
module load comp-intel/2018.3.222  # need this to call netDRMS 
source ~/.bashrc  # this puts conda in PATH
conda activate aia-on-pleiades
jupyter lab --no-browser --ip=$IP --port=8888
conda deactivate
module purge