"""
DRMS server configuration for local Ames netDRMS install
"""
import subprocess
import json
import warnings

import drms
from drms.config import ServerConfig

__all__ = ['ames_server', 'SubprocessClient', 'Client']


ames_server = ServerConfig(
    name='Ames',
    cgi_baseurl='/usr/local/netdrms/9.3.1/bin/linux_x86_64/',
    cgi_show_series='show_series',
    cgi_jsoc_info='jsoc_info'
)

# If on other nodes other than dfe1, have to call drms over SSH
ames_server_ssh = ServerConfig(
    name='Ames_ssh',
    cgi_baseurl=f'ssh dfe1 env LD_LIBRARY_PATH=$LD_LIBRARY_PATH {ames_server.cgi_baseurl}',
    cgi_show_series=ames_server.cgi_show_series,
    cgi_jsoc_info=ames_server.cgi_jsoc_info,
)


class SubprocessClient(object):

    def __init__(self, server, debug=False):
        if not isinstance(server, ServerConfig):
            raise ValueError('Server must be an instance of ServerConfig')
        self._server = server
        self.debug = debug

    @property
    def server(self,):
        return self._server

    @property
    def debug(self):
        return self._debug

    @debug.setter
    def debug(self, value):
        self._debug = True if value else False

    def _subprocess_request(self, query):
        r = subprocess.run(
            [query],
            shell=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        if r.stderr:
            warnings.warn(r.stderr.decode('utf-8'))
        # Strip the first two lines because they are not valid JSON
        return json.loads(''.join(r.stdout.decode('utf-8').split('\n')[2:]))

    def show_series(self, ds_filter):
        ds_filter = '' if ds_filter is None else ds_filter
        query = f'{self.server.url_show_series} -z {ds_filter}'
        return self._subprocess_request(query)

    def show_series_wrapper(self,):
        raise NotImplementedError()

    def series_struct(self, ds):
        query = f'{self.server.url_jsoc_info} op=series_struct ds={ds}'
        return self._subprocess_request(query)

    def rs_summary(self, ds):
        query = f'{self.server.url_jsoc_info} op=rs_summary ds={ds}'
        return self._subprocess_request(query)

    def rs_list(self, ds, key=None, seg=None, link=None, recinfo=False, n=None,
                uid=None):
        query = f'{self.server.url_jsoc_info} op=rs_list ds={ds}'
        if key is not None:
            query = f"{query} key={','.join(key)}"
        if seg is not None:
            query = f"{query} seg={','.join(seg)}"
        if link is not None:
            query = f"{query} link={','.join(link)}"
        if recinfo:
            query = f"{query} -R"
        if n is not None:
            query = f"{query} n={int(n)}"
        if uid is not None:
            query = f"{query} userhandle={uid}"
        query = f"{query} DRMS_QUERY_MEM=4096"  # Without this, very large queries fail
        return self._subprocess_request(query)

    def check_address(self,):
        raise NotImplementedError()

    def exp_request(self,):
        raise NotImplementedError()


class Client(drms.Client):

    def __init__(self, email=None, verbose=False, debug=False, ssh=False):
        self._json = SubprocessClient(ames_server_ssh if ssh else ames_server,
                                      debug=debug)
        self._info_cache = {}
        self.verbose = verbose  # use property for convertion to bool
        self.email = email      # use property for email validation
